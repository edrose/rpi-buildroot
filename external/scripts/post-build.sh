#!/bin/sh

set -u
set -e

# Add a console on tty1
if [ -e ${TARGET_DIR}/etc/inittab ]; then
    grep -qE '^tty1::' ${TARGET_DIR}/etc/inittab || \
	sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/sbin/getty -L  tty1 0 vt100 # HDMI console' ${TARGET_DIR}/etc/inittab
fi

# mount data and boot partitions
if [ -e ${TARGET_DIR}/etc/fstab ]; then
    echo -e "/dev/mmcblk0p1\t/mnt/boot\tvfat\tdefaults\t0\t2" >> ${TARGET_DIR}/etc/fstab
    echo -e "/dev/mmcblk0p4\t/mnt/data\text4\tdefaults\t0\t2" >> ${TARGET_DIR}/etc/fstab
fi
