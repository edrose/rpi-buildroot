################################################################################
#
# libcbor
#
################################################################################

LIBCBOR_VERSION = 0.5.0
LIBCBOR_SITE = https://github.com/PJK/libcbor/archive
LIBCBOR_SOURCE = v$(LIBCBOR_VERSION).tar.gz
LIBCBOR_LICENSE = MIT
LIBCBOR_LICENSE_FILES = LICENSE.md

LIBCBOR_INSTALL_STAGING = YES

# Also has CMake support, but that forces shared+static libs and static
# lib has a different name.
$(eval $(cmake-package))
